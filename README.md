## GSMC-Public ##

![GSMC Logo](https://bitbucket.org/gsmcenter/public/avatar/128)

`docker` `services` `manager` `gsmc` `gsm-center`

--------

***GSMC-Public*** is a web engine based on [Laravel](http://laravel.com) Framework


### Dependencies ###

Component | Tag | Version
----------|-----|--------
laravel | framework | 5.2
mongodb | org | 3.4
gsmc | public | 0.0.1



### Configuration ###

`Dockerfile` explaining its purpose:

```dockerfile
# Dockerizing MongoDB:  Dockerfile for building GSMC-Public image
# Based on ubuntu:16.04
# Installs MongoDB
```


### Deployment ###

##### PULL

```bash
docker pull tbaltrushaitis/gsmc-public
```

##### BUILD

```bash
docker build -c 512 --cpuset-cpus=0-1 -m 1024 --no-cache --pull --rm -f Docker.files/mongo/Dockerfile -t tbaltrushaitis/gsmc-public:latest .
```

##### COMMIT

```bash
docker commit 5db0dcd0d497
```

##### PUSH

```bash
docker push tbaltrushaitis/gsmc-public:0.0.2
```

--------

### Data storage configuration ###
```
TBD
```

--------

### How to run tests ###
```
TBD
```

--------

### Scaling instructions ###
```
TBD
```

--------

### Maintainers ###

* [Baltrushaitis Tomas](https://bitbucket.org/tbaltrushaitis)

> **Note:**  We're ready to get help in creation of tomorrow web ... maybe its you just come there as a new contributor?

--------

### Demo Site ###
[GSM Center](http://gsm-center.com.ua)

--------

#### **G**lobal **S**olutions **M**anagement **C**enter ####
